package modules

import (
	"gitlab.com/Vallyenfail/grpc-auth/internal/infrastructure/component"
	acontroller "gitlab.com/Vallyenfail/grpc-auth/internal/modules/auth/controller"
	aservice "gitlab.com/Vallyenfail/grpc-auth/internal/modules/auth/service"
	ucontroller "gitlab.com/Vallyenfail/grpc-auth/internal/modules/user/controller"
	"gitlab.com/Vallyenfail/grpc-auth/internal/modules/user/service"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontroller.Userer
}

func NewControllers(auth *aservice.Auth, users *service.UserServiceJSONRPC, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(auth, components)
	userController := ucontroller.NewUser(users, components)

	return &Controllers{
		Auth: authController,
		User: userController,
	}
}
